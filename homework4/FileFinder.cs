﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework4
{
    internal class FileFinder
    {
        public event EventHandler<FileArgs>? FileFound;
        private readonly string _path;

        public FileFinder(string path)
        {
            _path = path;
        }

        public async Task Process(CancellationToken cancellationToken = default(CancellationToken))
        {
            var fileNames = Directory.GetFiles(_path);
            foreach(var fileName in fileNames)
            {
                if (cancellationToken.IsCancellationRequested) { break; }
                FileFound?.Invoke(this, new FileArgs(Path.GetFileName(fileName)));
            }
        }
    }
}
