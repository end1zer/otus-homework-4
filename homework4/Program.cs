﻿using System;

namespace homework4
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            CancellationTokenSource cancellationToken = new();
            List<string> fileNames = new();

            var fileFinder = new FileFinder(Directory.GetCurrentDirectory());

            fileFinder.FileFound += FileFoundShowInfo;

            fileFinder.FileFound += ((objectSender, args) =>
            {
                fileNames.Add(args.Name);
            });

            await fileFinder.Process(cancellationToken.Token);

            Console.WriteLine("\nMax file name:");
            Console.WriteLine(fileNames.GetMax((string fileName) => fileName.Length));
        }

        private static void FileFoundShowInfo(object? sender, FileArgs args)
        {
            Console.WriteLine("File found: " + args.Name);
        }
    }
}