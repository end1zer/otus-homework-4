﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework4
{
    internal class FileArgs : EventArgs
    {
        public string Name { get; }

        public FileArgs(string name)
        {
            Name = name;
        }
    }
}
