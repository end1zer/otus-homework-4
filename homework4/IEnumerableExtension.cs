﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework4
{
    internal static class IEnumerableExtension
    {
        public static T? GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T : class
        {
            T? maxIterator = default(T);
            float? maxValue = null;

            foreach(var iterator in e)
            {
                var currentValue = getParameter(iterator);
                if (maxValue is null || currentValue > maxValue)
                {
                    maxValue = currentValue;
                    maxIterator = iterator;
                }
            }
            return maxIterator;
        }
    }
}
